<!DOCTYPE html>
<html>
<body>
	<h1>Git Cheat Sheet - a Collection of the Most Useful Commands</h1>
	<p>Git is the go-to version control tool for most software developers because it allows them to efficiently manage their source code and track file changes while working with a large team. In fact, Git has so many uses that memorizing its various commands can be a daunting task, which is why we’ve created this git cheat sheet.</p>
	<p>This guide includes an introduction to Git, a glossary of terms and lists of commonly used Git commands. Whether you’re having trouble getting started with Git, or if you’re an experienced programmer who just needs a refresher, you can refer to this cheat sheet for help.</p>
	<h1>What is Git?</h1>
	<p>If you work in web or software development, then you’ve probably used Git at some point. It remains the most widely used open source distributed version control system over a decade after its initial release. Unlike other version control systems that store a project’s full version history in one place, Git gives each developer their own repository containing the entire history of changes. While extremely powerful, Git has some complicated command line syntax that may be confusing at first. Nonetheless, once broken down they’re all fairly straightforward and easy to understand.</p>
	<h1>Git glossary</h1>
	<p>Before you get started with Git, you need to understand some important terms:</p>
	<h3>Branch</h3>
	<p>Branches represent specific versions of a repository that “branch out” from your main project. Branches allow you to keep track of experimental changes you make to repositories and revert to older versions.</p>
	<h3>Commit</h3>
	<p>A commit represents a specific point in your project’s history. Use the commit command in conjunction with the git add command to let git know which changes you wish to save to the local repository. Note that commits are not automatically sent to the remote server.</p>
	<h3>Checkout</h3>
	<p>Use the git checkout command to switch between branches. Just enter git checkout followed by the name of the branch you wish to move to, or enter git checkout master to return to the master branch. Mind your commits as you switch between branches.</p>
	<h3>Fetch</h3>
	<p>The git fetch command copies and downloads all of a branch’s files to your device. Use it to save the latest changes to your repositories. It’s possible to fetch multiple branches simultaneously.</p>
	<h3>Fork</h3>
	<p>A fork is a copy of a repository. Take advantage of “forking” to experiment with changes without affecting your main project.</p>
	<h3>Head</h3>
	<p>The commit at the tip of a branch is called the head. It represents the most current commit of the repository you’re currently working in.</p>
	<h3>Index</h3>
	<p>Whenever you add, delete or alter a file, it remains in the index until you are ready to commit the changes. Think of it as the staging area for Git. Use the git status command to see the contents of your index. Changes highlighted in green are ready to be committed while those in red still need to be added to staging.</p>
	<h3>Master</h3>
	<p>The master is the primary branch of all your repositories. It should include the most recent changes and commits.</p>
	<h3>Merge</h3>
	<p>Use the git merge command in conjunction with pull requests to add changes from one branch to another.</p>
	<h3>Origin</h3>
	<p>The origin refers to the default version of a repository. Origin also serves as a system alias for communicating with the master branch. Use the command git push origin master to push local changes to the master branch.</p>
	<h3>Pull</h3>
	<p>Pull requests represent suggestions for changes to the master branch. If you’re working with a team, you can create pull requests to tell the repository maintainer to review the changes and merge them upstream. The git pull command is used to add changes to the master branch.</p>
	<h3>Push</h3>
	<p>The git push command is used to update remote branches with the latest changes you’ve committed.</p>
	<h3>Rebase</h3>
	<p>The git rebase command lets you split, move or get rid of commits. It can also be used to combine two divergent branches.</p>
	<h3>Remote</h3>
	<p>A remote is a clone of a branch. Remotes communicate upstream with their origin branch and other remotes within the repository.</p>
	<h3>Repository</h3>
	<p>Git repositories hold all of your project’s files including branches, tags and commits.</p>
	<h3>Stash</h3>
	<p>The git stash command removes changes from your index and “stashes” them away for later. It’s useful if you wish to pause what you’re doing and work on something else for a while. You can’t stash more than one set of changes at a time.</p>
	<h3>Tags</h3>
	<p>Tags provide a way to keep track of important commits. Lightweight tags simply serve as pointers while annotated tags get stored as full objects.</p>
	<h3>Upstream</h3>
	<p>In the context of Git, upstream refers to where you push your changes, which is typically the master branch.</p>
	<p>See the Git docs reference guide for more in depth explanations of Git related terminology.</p>
	<h1>Commands for Configuring Git</h1>
	<p>Set the username:</p>
	<pre>git config –global user.name</pre>
	<p>Set the user email:</p>
	<pre>git config –global user.email</pre>
	<p>Create a Git command shortcut:</p>
	<pre>git config –global alias.</pre>
	<p>Set the preferred text editor:</p>
	<pre>git config –system core.editor</pre>
	<p>Open and edit the global configuration file in the text editor:</p>
	<pre>git config –global –edit</pre>
	<p>Enable command line highlighting:</p>
	<pre>git config –global color.ui auto</pre>
	<h1>Commands for Setting Up Git Repositories</h1>
	<p>Create an empty repository in the project folder:</p>
	<pre>git init</pre>
	<p>Clone a repository from GitHub and add it to the project folder:</p>
	<pre>git clone (repo URL)</pre>
	<p>Clone a repository to a specific folder:</p>
	<pre>git clone (repo URL) (folder)</pre>
	<p>Display a list of remote repositories with URLs:</p>
	<pre>git remote -v</pre>
	<p>Remove a remote repository:</p>
	<pre>git remote rm (remote repo name)</pre>
	<p>Retrieve the most recent changes from origin but don’t merge:</p>
	<pre>git fetch</pre>
	<p>Retrieve the most recent changes from origin and merge:</p>
	<pre>git pull</pre>
	<h1>Commands for Managing File Changes</h1>
	<p>Add file changes to staging:</p>
	<pre>git add (file name)</pre>
	<p>Add all directory changes to staging:</p>
	<pre>git add .</pre>
	<p>Add new and modified files to staging:</p>
	<pre>git add -A</pre>
	<p>Remove a file and stop tracking it:</p>
	<pre>git rm (file_name)</pre>
	<p>Untrack the current file:</p>
	<pre>git rm –cached (file_name)</pre>
	<p>Recover a deleted file and prepare it for commit:</p>
	<pre>git checkout <deleted file name></pre>
	<p>Display the status of modified files:</p>
	<pre>git status</pre>
	<p>Display a list of ignored files:</p>
	<pre>git ls-files –other –ignored –exclude-standard</pre>
	<p>Display all unstaged changes in the index and the current directory:</p>
	<pre>git diff</pre>
	<p>Display differences between files in staging and the most recent versions:</p>
	<pre>git diff –staged</pre>
	<p>Display changes in a file compared to the most recent commit:</p>
	<pre>git diff (file_name)</pre>
	<h1>Commands for Declaring Git Commits</h1>
	<p>Commit changes along with a custom message:</p>
	<pre>git commit -m "(message)"</pre>
	<p>Commit and add all changes to staging:</p>
	<pre>git commit -am "(message)"</pre>
	<p>Switch to a commit in the current branch:</p>
	<pre>git checkout <commit></pre>
	<p>Show metadata and content changes of a commit:</p>
	<pre>git show <code><commit></code></pre>
	<p>Discard all changes to a commit:</p>
	<pre>git reset –hard <code><commit></code></pre>
	<p>Discard all local changes in the directory:</p>
	<pre>git reset –hard Head</pre>
	<p>Show the history of changes:</p>
	<pre>git log</pre>
	<p>Stash all modified files:</p>
	<pre>git stash</pre>
	<p>Retrieve stashed files:</p>
	<pre>git stash pop</pre>
	<p>Empty stash:</p>
	<pre>git stash drop</pre>
	<p>Define a tag:</p>
	<pre>git tag (tag_name)</pre>
	<p>Push changes to origin:</p>
	<pre>git push</pre>
	<h1>Commands for Git Branching</h1>
	<p>Display a list of all branches:</p>
	<pre>git branch</pre>
	<p>Make a new branch and switch to it:</p>
	<pre>git checkout -b <code><branchname></code></pre>
	<p>Switch to a branch:</p>
	<pre>git checkout <code><branchname></code></pre>
	<p>Delete a branch:</p>
	<pre>git branch -d <code><branchname></code></pre>
	<p>Merge a different branch with your active branch:</p>
	<pre>git merge <code><branchname></code></pre>
	<p>Fetch a branch from the repository:</p>
	<pre>git fetch remote <code><branchname></code></pre>
	<p>View merge conflicts between branches:</p>
	<pre>git diff <code><sourcebranch> <targetbranch></code></pre>
	<p>Preview changes before merging branches:</p>
	<pre>git diff <code><sourcebranch> <targetbranch><code></pre>
	<p>Push all local branches to a designated remote repository:</p>
	<pre>git push –all</pre>
	<h1>Git Tips</h1>
	<p>Knowing all the Git commands won’t get you far if you don’t know how to make the most of them. Here are some version control best practices to follow:</p>
	<ol>
		<li>Commit Often<span><p>Keep your commits small by committing changes as often as possible. This makes it easier for team members to integrate their work without encountering merge conflicts.</p></span></li>
		<li>Test, Then Commit<span><p>Never commit incomplete work. Always test your changes before sharing your code with others.</p></span></li>
		<li>Use Commit Messages<span><p>Write commit messages to let other team members know what kind of changes you made. Be as descriptive as possible so your teammates know exactly what to look for.</p></span></li>
		<li>Branch Out<span><p>Take full advantage of branches to help you keep track of different lines of development. Don’t be afraid to go out on a limb and create a new branch to experiment with new features and ideas.</p></span></li>
		<li>Settle on a Common Workflow<span><p>There are several different ways to set up your Git workflow. Whichever one you choose, make sure you and your teammates are on the same page from the very beginning.</p></span></li>
	</ol>
	<h1>Summary</h1>
	<p>Unless you have an amazing photographic memory, memorizing every single Git command would be a challenge, so don’t bother trying to learn them all. You’ll always have this guide to reference when you need a specific command. You may even want to create your own Git cheat sheet with the commands you use most frequently.</p>
</body>
</html>